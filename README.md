 Veritaseum Simple Contract (VSC)

## Introduction

> A simple contract that allows a single deposit to an account and multiple withdrawals after 7 days

## Run Unit Tests
```sh
# Clone this repo and change into directory
cd kinda-erc20

# Install dependencies
npm install

# If necessary, install Truffle globally (Truffle v4.1.8 and Solidity v0.4.23)
npm install -g truffle

# Run the development console.
truffle develop

# Inside the truffle console; compile, migrate and run the unit tests
compile
migrate
test
```

Results: You should see 11 passing tests.

Possible Issues:
- If any of the tests fail, wait about 10 seconds for the development blockchain to mine the contract instance and rerun test
- When encountering migration errors, it is best to do a **migrate --reset** within the truffle console

## Web Demo
The web demo is basic UI allowing a user to interact with the Single Deposit contract.
```sh
# Clone this repo and change into directory
cd kinda-erc20

# Install dependencies
npm install

# If necessary, install Truffle globally
# Truffle v4.1.8 and Solidity v0.4.23 was used for development
npm install -g truffle

# Install ganache
npm install -g ganache-cli && ganache-cli

# In a separate window, compile and migrate the contract
truffle compile
truffle migrate --reset

# Start the web server
npm start
```
Navigate your browser to http://localhost:3000

## Docker Instructions
(Requires docker && docker-compose)
```sh
# Build docker image
docker build -t singledeposit .

# Start up demo containers
docker-compose up -d
```
Navigate your browser to http://localhost:3000
