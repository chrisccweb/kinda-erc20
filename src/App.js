import React, { Component } from 'react'
import getWeb3 from './utils/getWeb3'
import ContractInterface from './component/contractInterface'

import './css/oswald.css'
import './css/open-sans.css'
import './css/pure-min.css'
import './App.css'

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      web3: '',
      storageValue: 0,
      ledger: null,
      value: '',
      statusMessage: '',
      days: ''
    }

    // Event handlers
    this.handleChange   = this.handleChange.bind(this);
    this.handleDeposit  = this.handleDeposit.bind(this);
    this.handleWithdraw = this.handleWithdraw.bind(this);
    this.handleAddTime  = this.handleAddTime.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  async handleAddTime(event) {
    let status = await this.ledger.addTime(1)
    if(status) {
      this.setState({
        days: this.state.days + 1
      })
    }
  }

  async handleDeposit(event) {
    if(this.state.value === '') {
      this.showError('Please specify an amount')
      return false
    }

    let status = await this.ledger.deposit(this.state.web3.eth.accounts[0], this.state.value)
    if(status) {
      this.setState({
        storageValue: this.state.value, 
        value: ''
      })
    }
    else {
      this.showError('Unable to make another deposit')
    }
  }

  async handleWithdraw(event) {
    if(this.state.value === '') {
      this.showError('Please specify an amount')
      return false
    }

    let status = await this.ledger.withdraw(this.state.web3.eth.accounts[0], this.state.value)
    if(status) {
      let balance = this.state.storageValue - this.state.value
      this.setState({
        storageValue: balance, 
        value: ''
      })
    }
    else {
      this.showError('Unable to withdraw that amount')
    }
  }

  async componentWillMount() {
    try {
      let results = await getWeb3
      this.setState({web3: results.web3})
      await this.instantiateContract()
    } catch (error) {
      console.log(error.message)
    }
  }

  async instantiateContract() {
    this.ledger = new ContractInterface(this.state.web3)
    let from = this.state.web3.eth.accounts[0]
    try {
      let amount = await this.ledger.checkBalance(from)
      this.setState({storageValue: amount})
    } catch (error) {
      this.showError(error)
    }

    try {
      let days = await this.ledger.getElapsedTime()
      this.setState({ 
        days: days,
      })
    } catch (error) {
      this.showError(error)
    }
  }

  showError(errorMessage) {
    this.setState({statusMessage: errorMessage})
    setTimeout(() => {
      this.setState({statusMessage: ''})
    }, 3000);
  } 

  render() {
    return (
      <div className="App">
        <nav className="navbar pure-menu pure-menu-horizontal">
            <a href="#" className="pure-menu-heading pure-menu-link">Sandbox</a>
        </nav>

        <main className="container">
          <div className="pure-g">
            <div className="pure-u-1-1">
              <h2>Private Ledger</h2>
              <p>My Balance: {this.state.storageValue}</p>
              <form onSubmit={this.handleWithdraw}>
                <label>Amount: </label>
                <input type="text" placeholder="Digits only" value={this.state.value} onChange={this.handleChange} />
                <input type="button" onClick={this.handleAddTime}  value="Add a Day" />
                <input type="button" onClick={this.handleDeposit}  value="Deposit" />
                <input type="button" onClick={this.handleWithdraw} value="Withdraw" />
              </form>
              <div id='simulated-date'>
                <p>Simulation Day # {this.state.days}</p>
              </div>
              <div id='error-message'>
                <p>{this.state.statusMessage}</p>
              </div>
            </div>
          </div>
        </main>
      </div>
    );
  }
}

export default App