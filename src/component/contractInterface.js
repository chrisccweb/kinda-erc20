import SingleDepositContract from '../../build/contracts/SingleDeposit.json'
const contract = require('truffle-contract')
const singleDeposit = contract(SingleDepositContract)
let singleDepositInstance = ''
let web3 = ''

class ContractInterface {
    constructor(props) {
        web3 = props
        this.setupDefaults()
    }

    async setupDefaults() {
        singleDeposit.setProvider(web3.currentProvider)
        singleDeposit.defaults({from: web3.eth.coinbase});
    }

    addTime(days) {
        return new Promise(async resolve => {
            singleDepositInstance = await singleDeposit.deployed()
            try {
                let currentTime = await singleDepositInstance.getTime()
                await singleDepositInstance.addTime(days)
                let nextTime = await singleDepositInstance.getTime()
                resolve(nextTime - currentTime === 86400)
            } catch (error) {
                console.log('Clock error: ' + error.message)
                resolve(false)
            }
        }) 
    }

    getElapsedTime() {
        return new Promise(async resolve => {
            singleDepositInstance = await singleDeposit.deployed()
            try {
                let result = await singleDepositInstance.getElapsedTime()
                resolve(Math.round(result.toNumber()/86400) + 1)
            } catch (error) {
                console.log(error.message)
                resolve(false)
            }
        }) 
    }

    checkBalance(from) {
        return new Promise(async resolve => {
            singleDepositInstance = await singleDeposit.deployed()
            try {
                let result = await singleDepositInstance.checkBalance.call({from: from})
                resolve(result.toNumber())
            } catch (error) {
                console.log(error.message)
                resolve(false)
            }
        }) 
    }

    deposit(from, amount) {
        return new Promise(async resolve => {
            singleDepositInstance = await singleDeposit.deployed()
            try {
                let result = await singleDepositInstance.deposit(amount, {from: from, value: amount})
                resolve(result)
            } catch (error) {
                resolve(false)
            }
        }) 
    }

    withdraw(from, amount) {
        return new Promise(async resolve => {
            singleDepositInstance = await singleDeposit.deployed()
            try {
                let result = await singleDepositInstance.withdraw(amount, {from: from})
                let status = (result.receipt.status === 1) || (result.receipt.status.indexOf('01'))
                resolve(status)
            } catch (error) {
                resolve(false)
            }
        }) 
    }
}
  
export default ContractInterface  