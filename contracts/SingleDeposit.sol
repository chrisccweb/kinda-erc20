pragma solidity ^0.4.18;

contract SingleDeposit {
  // VARIABLES
  bool debugMode = false;
  uint creationTime = now;
  uint currentTime = now;
  mapping (address => uint) balances;
  mapping (address => uint) deposits;

  // EVENTS
  event Transfer(address indexed _from, address indexed _to, uint256 _value);

  // CORE
  function deposit(uint amount) payable public {
    // Ensure this is a first time deposit
    require(deposits[msg.sender] == 0);

    // Ensure the deposit is valid amount
    require(amount > 0);

    // No need to increment the amount because this is a single deposit function
    balances[msg.sender] = amount;

    // Set deposit timestamp
    deposits[msg.sender] = currentTime;

    emit Transfer(msg.sender, this, amount);
  }

  function withdraw(uint amount) public {
    /*
      Issues spotted:
      1. If withdrawal was called by an intermediate contract, "msg.sender" is not always equal to "tx.origin". Best to use msg.sender.
      2. The withdraw function only allow withdrawals that are less than 7 days old. The balance is also inaccessible after 7 days
      3. The withdraw function does not allow full withdrawal (leaving a zero balance)
      4. Should update balance before sending amount (Protects agains reentrancy)
      5. Also prefer to use transfer vs call for default low gas limits
      6. Should also send the withdrawal amount instead of the full balance
    */

    if (debugMode) {
      require(now > depositTime + 15 seconds);
    } else {
      uint depositTime = deposits[msg.sender]; // Fixed #1
      require(currentTime >= depositTime + 7 days); // Fixed #2
    }

    uint balance = balances[msg.sender]; // Fixed #1
    require(balance >= amount); // Fixed #3

    /*
    // VULNERABLE TO REENTRANCY
    if(msg.sender.call.value(balance)()) { // OR if(msg.sender.call.value(balance).gas(safeGasLimit)()) {
      balances[msg.sender] -= amount;
    }
    */

    // Fixed #4, #5, #6
    emit Transfer(this, msg.sender, amount);
    balances[msg.sender] -= amount; 
    msg.sender.transfer(amount);
  }

  function checkBalance() public view returns (uint) {
    return balances[msg.sender];
  }

  // FALLBACKS
  function() public payable {}

  // HELPERS
  function getTime() public view returns(uint) {
    return currentTime;
  }

  function getElapsedTime() public view returns(uint) {
    return currentTime - creationTime;
  }

  function addTime(uint daysAfter) public {
    require(int(daysAfter) > 0);
    currentTime += daysAfter * 1 days;
  }

}