const truffleAssert = require('truffle-assertions');

var SingleDeposit = artifacts.require("SingleDeposit")

beforeEach(async () => {
  simpleStorageInstance = await SingleDeposit.new()
})

contract('SingleDeposit', async accounts => {

  it("VIRTUAL CALENDAR: Should set the virtual calendar and then read the virtual time after a time delay", async () => {
    let currentTime = await simpleStorageInstance.getTime()
    currentTime = currentTime.toNumber()

    let testCurrentTime = Math.floor(Date.now()/1000)
    let status = (testCurrentTime === currentTime) || (testCurrentTime === currentTime + 1) 
    assert.equal(status, true, "Failed to get accurate current time")

    await sleep(2000)

    let nextCurrentTime = await simpleStorageInstance.getTime()
    assert.equal(nextCurrentTime, currentTime, "Failed to get accurate current time. No time should have elapsed")
  })

  it("VIRTUAL CALENDAR: Attempt to subtract day(s) from the virtual calendar.", async () => {
    for (let index = -5; index < 0; index++) {
      let currentTime = await simpleStorageInstance.getTime()
      currentTime = currentTime.toNumber()

      try {
        let nextDay = await simpleStorageInstance.addTime(index)
        let nextDayTime = await simpleStorageInstance.getTime()
        assert.equal(nextDayTime.toNumber() - currentTime, 86400 * index, "Failed to add a day to virtual calendar")
      } catch (error) {
        assert.equal(error.message, 'VM Exception while processing transaction: revert', 'Only positive increment allowed!')
      }
    }
  })

  it("VIRTUAL CALENDAR: Attempt to add day(s) to the virtual calendar.", async () => {
    for (let index = 1; index < 10; index++) {
      let currentTime = await simpleStorageInstance.getTime()
      currentTime = currentTime.toNumber()

      try {
        let nextDay = await simpleStorageInstance.addTime(index)
        let nextDayTime = await simpleStorageInstance.getTime()
        assert.equal(nextDayTime.toNumber() - currentTime, 86400 * index, "Failed to add a day to virtual calendar")
      } catch (error) {
        assert.equal(error.message, 'VM Exception while processing transaction: revert', 'Only positive increment allowed!')
      }
    }
  })

  it("VIRTUAL CALENDAR: Attempt to add 0 days to the virtual calendar.", async () => {
    let currentTime = await simpleStorageInstance.getTime()
    currentTime = currentTime.toNumber()

    try {
      let nextDay = await simpleStorageInstance.addTime(0)
      let nextDayTime = await simpleStorageInstance.getTime()
      assert.equal(nextDayTime.toNumber() - currentTime, 86400 * index, "Failed to add a day to virtual calendar")
    } catch (error) {
      assert.equal(error.message, 'VM Exception while processing transaction: revert', 'Only positive integers allowed!')
    }
  })
 
  it("VIRTUAL CALENDAR: Attempt to add a non-integer to the virtual calendar.", async () => {
    let input = 'one'
    let currentTime = await simpleStorageInstance.getTime()
    currentTime = currentTime.toNumber()

    try {
      let nextDay = await simpleStorageInstance.addTime(input)
      let nextDayTime = await simpleStorageInstance.getTime()
      assert.equal(nextDayTime.toNumber() - currentTime, 86400 * index, "Failed to add a day to virtual calendar")
    } catch (error) {
      assert.equal(error.message, 'new BigNumber() not a number: ' + input.toString(), 'Only positive integers allowed!')
    }

    input = true
    try {
      let nextDay = await simpleStorageInstance.addTime(input)
      let nextDayTime = await simpleStorageInstance.getTime()
      assert.equal(nextDayTime.toNumber() - currentTime, 86400 * index, "Failed to add a day to virtual calendar")
    } catch (error) {
      assert.equal(error.message, 'new BigNumber() not a number: ' + input.toString(), 'Only positive integers allowed!')
    }

    try {
      let nextDay = await simpleStorageInstance.addTime(false)
      let nextDayTime = await simpleStorageInstance.getTime()
      assert.equal(nextDayTime.toNumber() - currentTime, 86400 * index, "Failed to add a day to virtual calendar")
    } catch (error) {
      assert.equal(error.message, 'VM Exception while processing transaction: revert', 'Only positive integers allowed!')
    }
  })
  
  it("DEPOSIT: Should make a deposit of 1000, validate the balance.", async () => {
    let amount = 1000
    let contractAddress = simpleStorageInstance.contract.address

    let status = await simpleStorageInstance.deposit(amount, {from: accounts[0], value: amount})
    assert.equal(status.receipt.status, 1, "Failed to deposit amount")
    balance = await simpleStorageInstance.checkBalance.call({from: accounts[0]})
    assert.equal(balance.toNumber(), amount, 'Failed the balance check')
  })

  it("DEPOSIT: Should attempt 2 deposits of 1000 Weis to the same account and validate a single deposit", async () => {
    let balance = 0
    let amount = 1000

    let status = await simpleStorageInstance.deposit(amount, {from: accounts[0], value: amount})
    assert.equal(status.receipt.status, 1, "Failed to deposit amount")

    try {
      let status = await simpleStorageInstance.deposit(amount, {from: accounts[0], value: amount})
      throw new Error('Expecting a failed deposit attempt')
    } catch (error) {
      if(error.message.indexOf('VM Exception while processing transaction: revert') === -1) {
        assert.notEqual(error.message, 'Expecting a failed deposit attempt', 'Expecting a failed deposit attempt')
      }
    }
  })

  it("WITHDRAWAL: Deposit 100 and withdraw 100 Weis after 1, 2, 3 ... 10 days of deposit.", async() => {
    let depositAmount = 100
    let withdrawAmount = 1
    let futureDays = 1

    let status = await simpleStorageInstance.deposit(depositAmount, {from: accounts[0], value: depositAmount})
    assert.equal(status.receipt.status, 1, "Failed to deposit amount")
    let balance = await simpleStorageInstance.checkBalance.call({from: accounts[0]})
    assert.equal(balance.toNumber(), depositAmount, 'Failed the balance check')

    for (let index = 1; index <= 10; index++) {
      let nextDay = await simpleStorageInstance.addTime(1)

      // Attempt early withdrawals
      try {
        let status = await simpleStorageInstance.withdraw(withdrawAmount, {from: accounts[0]})
        let balance = await simpleStorageInstance.checkBalance.call({from: accounts[0]})
        depositAmount -= (index >= 7) ? withdrawAmount : 0
        assert.equal(balance.toNumber(), depositAmount, 'Early withdrawal not allowed')
      } catch (error) {
        if(index < 7) {
          assert.equal(error.message, 'VM Exception while processing transaction: revert', 'Something went wrong with a withdrawal request!')
        }
      }
    }
  })

  it("WITHDRAWAL: Attempt to withdraw exact balance amount after 7 days", async() => {
    let depositAmount  = 100
    let withdrawAmount = 100
    let contractAddress = simpleStorageInstance.contract.address

    let status = await simpleStorageInstance.deposit(depositAmount, {from: accounts[0], value: depositAmount})
    assert.equal(status.receipt.status, 1, "Failed to deposit amount")
    let balance = await simpleStorageInstance.checkBalance.call({from: accounts[0]})
    assert.equal(balance.toNumber(), depositAmount, 'Failed the balance check')

    let eightDaysLater = await simpleStorageInstance.addTime(8)

    let withdrawStatus = await simpleStorageInstance.withdraw(withdrawAmount, {from: accounts[0]})
    balance = await simpleStorageInstance.checkBalance.call({from: accounts[0]})
    assert.equal(balance.toNumber(), 0, 'Depositor prevented from withdrawing full balance')
  })

  it("WITHDRAWAL: Attempt to overdraw an account after 7 days", async() => {
    let depositAmount  = 100
    let withdrawAmount = 101
    let eightDaysLater = 8

    let status = await simpleStorageInstance.deposit(depositAmount, {from: accounts[0], value: depositAmount})
    assert.equal(status.receipt.status, 1, "Failed to deposit amount")
    let balance = await simpleStorageInstance.checkBalance.call({from: accounts[0]})
    assert.equal(balance.toNumber(), depositAmount, 'Failed the balance check')

    try {
      let eightDaysLaterStatus = await simpleStorageInstance.addTime(eightDaysLater)
      status = await simpleStorageInstance.withdraw(withdrawAmount, {from: accounts[0]})
      balance = await simpleStorageInstance.checkBalance.call({from: accounts[0]})
      assert.equal(balance.toNumber(), depositAmount, 'Early withdrawal not allowed')
    } catch (error) {
      assert.equal(error.message, 'VM Exception while processing transaction: revert', 'An overdraft was allowed!')
    }

  })

  it("EVENT: Check for deposit and withdraw events", async() => {
    let depositAmount  = 100
    let withdrawAmount = 100
    let contractAddress = simpleStorageInstance.contract.address

    let status = await simpleStorageInstance.deposit(depositAmount, {from: accounts[0], value: depositAmount})
    assert.equal(status.receipt.status, 1, "Failed to deposit amount")
    let balance = await simpleStorageInstance.checkBalance.call({from: accounts[0]})
    assert.equal(balance.toNumber(), depositAmount, 'Failed the balance check')

    truffleAssert.eventEmitted(status, 'Transfer', ev => {
      let from  = ev._from 
      let to    = ev._to 
      let value = ev._value.toNumber()
      assert.equal(accounts[0], from, 'Broadcasted incorrect originator')
      assert.equal(to, contractAddress, 'Broadcasted incorrect contract address')
      assert.equal(value, depositAmount, 'Broadcasted incorrect deposit amount')
      return true
    })

    let eightDaysLater = await simpleStorageInstance.addTime(8)

    let withdrawStatus = await simpleStorageInstance.withdraw(withdrawAmount, {from: accounts[0]})
    truffleAssert.eventEmitted(withdrawStatus, 'Transfer', ev => {
      let from  = ev._from 
      let to    = ev._to 
      let value = ev._value.toNumber()
      assert.equal(contractAddress, from, 'Broadcasted incorrect originator')
      assert.equal(to, accounts[0], 'Broadcasted incorrect deposit amount')
      assert.equal(value, withdrawAmount, 'Broadcasted incorrect deposit amount')
      return true
    })

    balance = await simpleStorageInstance.checkBalance.call({from: accounts[0]})
    assert.equal(balance.toNumber(), 0, 'Depositor prevented from withdrawing full balance')
  })

})

let sleep = ms => {
  return new Promise(resolve => setTimeout(resolve, ms));
}

let addTime = days => {
  return now + days * 86400000
}